require "oneday/reader/entry"

module Oneday
  class Reader

    # Will initialize the Oneday::Reader.
    def initialize
    end

    # Will list existing entries based on the configured storage path.
    def list
    end

    # Will open a given entry.
    def open(entry=nil)
    end

  end
end